﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebApplication1.Controllers
{
    //[Authorize]
    public class ValuesController : ApiController
    {

        
        public class doctor
        {
            // doctor infor
            public string doctor_id { set; get; }
            public string doctor_name { set; get; }
            // specs
            public string specialest_id { set; get; }
            public string specialest_name { set; get; }
        }


        // GET api/values
        public List<doctor> Get()
        {
            return getDoctors();
        }

        List<doctor> getDoctors()
        {
            var list = new List<doctor>();
            string q = "SELECT doctor_id,doctor_name,specialest_id,specialest_name " +
                              "FROM hospital_demo_zero.doctor,hospital_demo_zero.doctor_specialest,hospital_demo_zero.doctor_specialest_has_doctor " +
                              "where " +
                              "doctor_id = doctor_doctor_id " +
                              "&& specialest_id = doctor_specialest_specialest_id";
            var con = new MySqlConnection(connectionString);
            using (var command = new MySqlCommand(q, con))
            {
                con.Open();
                using (var reader = command.ExecuteReader())
                {
                    
                    while (reader.Read())
                        list.Add(new doctor { doctor_id = reader.GetString(0), doctor_name = reader.GetString(1), specialest_id = reader.GetString(2), specialest_name = reader.GetString(3) });
                     
                }
            }
            return list;
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }
        string connectionString = "user id=root;password=root;persistsecurityinfo=True;server=localhost;database=hospital_demo_zero";

        // POST api/values
        public int Post([FromBody]string value)
        {
            log("starting post" + value);
            MySqlConnection conn = new MySqlConnection(connectionString);
            conn.Open();
            MySqlCommand cmd = new MySqlCommand("Insert into hospital_demo_zero.doctor (docror_name )values(@docror_name); SELECT LAST_INSERT_ID();", conn);

            cmd.Parameters.AddWithValue("@docror_name", "adam ali");
            int currentID = Convert.ToInt32(cmd.ExecuteScalar());
            cmd.Dispose();
            conn.Close();
            log("end post" + value);
            return currentID;
        }



        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
        void log(string message)
        {
            Console.WriteLine(message);
        }
    }
}
